import express, { Express, Request, Response } from 'express';
import { UserRepository } from './services/userRepository';
import { ConflictUpdateUserResult, DuplicateAddUserResult, InvalidIdUpdateUserResult, SuccessAddUserResult, SuccessUpdateUserResult, UserService } from './services/userService';
import { User } from './services/user';

const userRepository = new UserRepository();
const userService = new UserService(
    userRepository
);

const app: Express = express();
app.use(express.json());

app.get('/', (req: Request, res: Response) => {
    res.send('Express + TypeScript Server!');
});

interface ListUserResponse {
    _version: string;
    id: number;
    name: string;
    age: number;
}

app.get('/users/list', async (req: Request, res: Response<ListUserResponse[]>) => {
    const result = await userRepository.list();
    const users = result.map(x => {
        return {
            ...x.result,
            _version: x.version
        }
    });
    res.json(users);
});

interface ErrorResponse {
    error: string;
}

interface AddUserRequest {
    name: string|null;
    age: number|null;
}

interface SuccessAddUserResponse {
    _version: string;
    id: number;
    name: string;
    age: number;
}

app.post('/users/add', async (req: Request<{}, {}, AddUserRequest>, res: Response<ErrorResponse|SuccessAddUserResponse>) => {
    if (req.body.name == null ||
        req.body.name == "" ||
        req.body.age == null ||
        req.body.age <= 0
    ) {
        res.status(400).json({
            error: "invalid request"
        });
        return;
    }

    const result = await userService.add(
        req.body.name,
        req.body.age
    );

    if (result instanceof SuccessAddUserResult) {
        res.json({
            ...result.user,
            _version: result.version
        });
    } else if (result instanceof DuplicateAddUserResult) {
        res.json({
            error: "duplicate user"
        })
    } else {
        throw new Error("Unhandled result type");
    }
});

interface UpdateUserRequest {
    id: number|null;
    name: string|null;
    age: number|null;
    _version: string|null;
}

interface SuccessUpdateUserResponse {
    _version: string;
}

interface ConflictUpdateUserResponse {
    error: string;
    _version: string;
    id: number;
    name: string;
    age: number;
}

app.post('/users/update', async (req: Request<{}, {}, UpdateUserRequest>, res: Response<ErrorResponse|SuccessUpdateUserResponse|ConflictUpdateUserResponse>) => {
    if (req.body.id == null ||
        req.body.name == null ||
        req.body.name == "" ||
        req.body.age == null ||
        req.body.age <= 0 ||
        req.body._version == null
    ) {
        res.status(400).json({
            error: "invalid request"
        });
        return;
    }

    const update = new User(
        req.body.id,
        req.body.name,
        req.body.age
    );
    const result = await userService.update(
        update,
        req.body._version
    );

    if (result instanceof SuccessUpdateUserResult) {
        const successResult = result as SuccessUpdateUserResult;
        res.json({
            _version: successResult.newVersion
        });
    } else if (result instanceof InvalidIdUpdateUserResult) {
        res.status(400).json({
            error: "invalid id"
        });
    } else if (result instanceof ConflictUpdateUserResult) {
        const conflictResult = result as ConflictUpdateUserResult;
        res.status(400).json({
            error: "conflict",
            _version: conflictResult.newVersion,
            ...conflictResult.user
        });
    } else {
        throw new Error("Unhandled result type");
    }
});

const port = parseInt(process.env["APP_PORT"] ?? "3000");
app.listen(port, () => {
    console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});
