import sql, { Request } from 'mssql';

const PORT = process.env['DB_PORT'] == null ?
    undefined :
    parseInt(process.env['DB_PORT']);

const POOL = new sql.ConnectionPool({
    server: process.env['DB_SERVER'] ?? "",
    port: PORT,
    database: process.env['DB_DATABASE'],
    user: process.env['DB_USER'],
    password: process.env['DB_PASSWORD'],
    options: {
        trustServerCertificate: true
    }
});
let connected = false;

export async function getConnection() : Promise<Request> {
    if (!connected) {
        await POOL.connect();
        connected = true;
    }

    return POOL.request();
}
