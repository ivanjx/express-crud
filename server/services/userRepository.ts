import { getConnection } from "./dbConn";
import { VResult } from "./vresult";
import { User } from "./user";
import sql from "mssql";

export interface IUserRepository {
    list(): Promise<VResult<User>[]>;
    get(id: number): Promise<VResult<User> | null>;
    add(name: string, age: number): Promise<VResult<User>>;
    update(user: User, currentVersion: string): Promise<boolean>;
}

export class UserRepository implements IUserRepository {
    async list(): Promise<VResult<User>[]> {
        const conn = await getConnection();
        const result = await conn.query(
            "SELECT id, name, age, _version FROM users"
        );
        return result.recordset.map(row => {
            const user = new User(
                row["id"] as number,
                row["name"] as string,
                row["age"] as number
            );
            return new VResult<User>(
                user,
                row["_version"] as Buffer
            );
        });
    }

    async get(id: number): Promise<VResult<User> | null> {
        const conn = await getConnection();
        const result = await conn
            .input("id", sql.Int, id)
            .query(
                "SELECT id, name, age, _version FROM users " +
                "WHERE id = @id"
            );
        
        if (result.recordset.length == 0) {
            return null;
        }

        const row = result.recordset[0];
        const user = new User(
            row["id"] as number,
            row["name"] as string,
            row["age"] as number
        );
        return new VResult<User>(
            user,
            row["_version"] as Buffer
        );
    }

    async add(name: string, age: number): Promise<VResult<User>> {
        const conn = await getConnection();
        const result = await conn
            .input("name", sql.VarChar, name)
            .input("age", sql.Int, age)
            .query(
                "INSERT INTO users (name, age) " +
                "OUTPUT INSERTED.id " +
                "VALUES (@name, @age)"
            );
        
        if (result.rowsAffected[0] != 1 ||
            result.recordset.length != 1) {
            throw new Error("Unable to save user");
        }

        const newId = result.recordset[0]["id"] as number;
        const newUser = await this.get(newId);

        if (newUser == null) {
            throw new Error("Unable to get new user");
        }

        return newUser;
    }

    async update(user: User, currentVersion: string): Promise<boolean> {
        const conn = await getConnection();
        const result = await conn
            .input("id", sql.Int, user.id)
            .input("name", sql.VarChar, user.name)
            .input("age", sql.Int, user.age)
            .input("version", sql.VarBinary, VResult.deserialize(currentVersion))
            .query(
                "UPDATE users SET " +
                "name = @name, " +
                "age = @age " +
                "WHERE id = @id AND _version = @version"
            );
        
        if (result.rowsAffected[0] != 1) {
            return false;
        }

        return true;
    }
}
