import { User } from "./user";
import { IUserRepository } from "./userRepository";

export abstract class AddUserResult {}

export class SuccessAddUserResult extends AddUserResult {
    user: User;
    version: string;

    constructor(user: User, version: string) {
        super();
        this.user = user;
        this.version = version;
    }
}

export class DuplicateAddUserResult extends AddUserResult {}

export abstract class UpdateUserResult {}

export class SuccessUpdateUserResult {
    newVersion: string;

    constructor(newVersion: string) {
        this.newVersion = newVersion;
    }
}

export class InvalidIdUpdateUserResult {}

export class ConflictUpdateUserResult {
    user: User;
    newVersion: string;

    constructor(user: User, newVersion: string) {
        this.user = user;
        this.newVersion = newVersion;
    }
}

export interface IUserService {
    add(name: string, age: number): Promise<AddUserResult>;
    update(user: User, currentVersion: string): Promise<UpdateUserResult>;
}

export class UserService implements IUserService {
    userRepo: IUserRepository;

    constructor(userRepo: IUserRepository) {
        this.userRepo = userRepo;
    }

    async add(name: string, age: number): Promise<AddUserResult> {
        // Perform validation or anything here.
        // For example checking if the name already exists, etc.
        // This is just an example:
        if (name == "john doe") {
            return new DuplicateAddUserResult();
        }
        
        // If no logic to perform then just call the repository
        // directly from the controller.
        const result = await this.userRepo.add(name, age);
        return new SuccessAddUserResult(
            result.result,
            result.version
        );
    }

    async update(user: User, currentVersion: string): Promise<UpdateUserResult> {
        // Perform validation or anything.
        let currentUser = await this.userRepo.get(user.id);

        if (currentUser == null) {
            return new InvalidIdUpdateUserResult();
        }

        // Attempting to save.
        const success = await this.userRepo.update(
            user,
            currentVersion
        );
        currentUser = await this.userRepo.get(user.id);

        if (currentUser == null) {
            throw new Error("Invalid state");
        }

        if (!success) {
            // NOTE:
            // Here you may implement other reconciliation method such as
            // force write or something else according to your business needs.
            // In this case i will just notify the end user.
            return new ConflictUpdateUserResult(
                currentUser.result,
                currentUser.version);
        }

        return new SuccessUpdateUserResult(currentUser.version);
    }
}
