export class VResult<T> {
    result: T;
    version: string;

    constructor(result: T, version: any) {
        this.result = result;

        if (!(version instanceof Buffer)) {
            throw new Error("Invalid version type");
        }

        this.version = (version as Buffer).toString("base64");
    }

    static deserialize(num: string): Buffer {
        return Buffer.from(num, "base64");
    }

    deserialize(): Buffer {
        return VResult.deserialize(this.version);
    }
}
